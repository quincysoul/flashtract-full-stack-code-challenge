# Developer

## Things I'd like verified by flashtract best practices (would be a quick slack / zoom)
- TypeORM seems to agnostically allow many-to-one to be done in multiple ways. Which would we pref
at flashtract? (I went with cascade and annotated as @ManyToOne).
- Reference: https://orkhan.gitbook.io/typeorm/docs/relations
    - @ManyToOne @OneToMany with {cascade: true}
        - This appears to handle `relations` automatically (automagically)
        - The pro - less code, the con - less control of what's occurring.
    - @JoinTable to let typeorm handle this relation with a little more 'granular' control
    - Manual join table statement when fetching / updating data.
- How much unit testing we want on each aspect of nest.js files in prod
- Exception handling in typeorm. I'd like to see how flashtract does it / best practices. It does give the impression of built in error handling so I'd like to see *where* it is absolutely required we do it.
- I think something was mentioned about an automatic swagger generator? Would be useful obviously.

## Some TODOs for production code (time boxed out for takehome)
- Unit testing. 
- Schema validation /sanitize for user input on backend
- Logging.

## 1. D.C. fullstack js dev - strong node.js
## 100% Remote direct hire HERETTO
Small company 45 employees. Very modern typescript react frontend, node, java modern
Full time salary.
175k total compo.