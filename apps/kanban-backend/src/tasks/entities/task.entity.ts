import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, BaseEntity, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { CategoryEntity } from '../../categories/entities/category.entity';

@Entity()
export class TaskEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    name: string;

    @Column()
    description: string;

    @CreateDateColumn()
    createdDate: Date;

    @ManyToOne(() => CategoryEntity, category => category.tasks, {
        cascade: ['insert', 'update']
    })
    category: CategoryEntity;

    @UpdateDateColumn()
    updatedDate: Date;
}