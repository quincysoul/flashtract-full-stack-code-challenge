import { PartialType } from '@nestjs/mapped-types';
import { CategoryEntity } from '../../categories/entities/category.entity';
import { CreateTaskDto } from './create-task.dto';

export class UpdateTaskDto extends PartialType(CreateTaskDto) {
    id: string;
    name?: string;
    description?: string;
    category?: CategoryEntity;
}
