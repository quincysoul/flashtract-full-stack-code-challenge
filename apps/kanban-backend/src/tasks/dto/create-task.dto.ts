import { CategoryEntity } from "../../categories/entities/category.entity";

export class CreateTaskDto {
    name: string;
    description?: string;
    category?: CategoryEntity;
}
