import { Injectable } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { TaskEntity } from './entities/task.entity';
import { CategoryEntity } from '../categories/entities/category.entity';
import { CategoriesService } from '../categories/categories.service';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(TaskEntity)
    private tasksRepository: Repository<TaskEntity>,
    // @InjectRepository(CategoryEntity)
    // private categoriesRepository: Repository<CategoryEntity>
    private categoriesService: CategoriesService
  ) { }

  async insert(taskDetails: CreateTaskDto): Promise<TaskEntity> {
    let taskEntity: TaskEntity = TaskEntity.create();
    const { name, description, category = null } = taskDetails;
    taskEntity.name = name;
    taskEntity.description = description;
    console.log("category of dto", category)
    const categoryEntity: CategoryEntity = await this.categoriesService.findOne(category.id)
    console.log("FOUND categoryEntity", categoryEntity)
    taskEntity.category = categoryEntity;
    // const relationcat = await this.tasksRepository.find({ relations: ["category"] })
    // console.log("relationcat:", relationcat)
    await TaskEntity.save(taskEntity);
    return taskEntity;
  }

  async findAll(): Promise<TaskEntity[]> {
    return this.tasksRepository.find();
  }

  async findOne(id: string): Promise<TaskEntity> {
    // const relationcat = await this.tasksRepository.find({ id, relations: ["category"] })
    // console.log("the relationcat::", relationcat)
    return this.tasksRepository.findOne(id, { relations: ["category"] });
  }

  async remove(id: string): Promise<DeleteResult> {
    return await this.tasksRepository.delete(id);
  }

  async update(updateTaskDto: UpdateTaskDto): Promise<UpdateResult> {
    const { id } = updateTaskDto;
    return await this.tasksRepository.update(id, updateTaskDto)
  }
}