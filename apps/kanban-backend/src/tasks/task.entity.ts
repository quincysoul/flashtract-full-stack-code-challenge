// import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn, ManyToOne } from 'typeorm';
// import Category from '../categories/category.entity';

// @Entity()
// class Task {
//   @PrimaryGeneratedColumn()
//   id: number;

//   @ManyToOne(() => Category, category => category.id)
//   category: Category;

//   @Column()
//   name: string;

//   @Column()
//   description: string;

// }
// /*
// const photo1 = new Photo();
// photo1.url = "me.jpg";
// await connection.manager.save(photo1);

// const photo2 = new Photo();
// photo2.url = "me-and-bears.jpg";
// await connection.manager.save(photo2);

// const user = new User();
// user.name = "John";
// user.photos = [photo1, photo2];
// await connection.manager.save(user);

// const users = await connection
//     .getRepository(User)
//     .createQueryBuilder("user")
//     .leftJoinAndSelect("user.photos", "photo")
//     .getMany();

// // or from inverse side

// const photos = await connection
//     .getRepository(Photo)
//     .createQueryBuilder("photo")
//     .leftJoinAndSelect("photo.user", "user")
//     .getMany();

// */

// export default Task;