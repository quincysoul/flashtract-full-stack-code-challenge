import { Entity, Column, PrimaryGeneratedColumn, OneToMany, BaseEntity } from 'typeorm';
import { TaskEntity } from '../../tasks/entities/task.entity';

@Entity()
export class CategoryEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  description: string;

  // [TODO]: On frontend, do not write null order. Base on the current order situation (min/max).
  @Column({ nullable: true })
  order: number;

  @OneToMany(() => TaskEntity, task => task.category,
    {
      // cascade: true
    }
  )
  tasks: TaskEntity[];
}