import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { CategoryEntity } from './entities/category.entity';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(CategoryEntity)
    private categoriesRepository: Repository<CategoryEntity>,
  ) { }

  async insert(categoryDetails: CreateCategoryDto): Promise<CategoryEntity> {
    let categoryEntity: CategoryEntity = CategoryEntity.create();
    const { name, description } = categoryDetails;
    categoryEntity.name = name;
    categoryEntity.description = description;
    await CategoryEntity.save(categoryEntity);
    return categoryEntity;
  }

  async findAll(): Promise<CategoryEntity[]> {
    return await this.categoriesRepository.find({ relations: ["tasks"] });
  }

  async findOne(id: string): Promise<CategoryEntity> {
    return await this.categoriesRepository.findOne(id);
  }

  async remove(id: string): Promise<DeleteResult> {
    return await this.categoriesRepository.delete(id);
  }

  async update(updateCategoryDto: UpdateCategoryDto): Promise<UpdateResult> {
    const { id } = updateCategoryDto;
    return await this.categoriesRepository.update(id, updateCategoryDto)
  }
}