export class CreateCategoryDto {
    name: string;
    description?: string;
    order?: number;
}
