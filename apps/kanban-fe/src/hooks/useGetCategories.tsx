import axios, { CancelToken } from 'axios';
import { useCallback, useEffect, useState } from 'react';
import { TaskData } from '../types/TaskData';

export interface CategoryData {
  id: string
  name: string
  description: string
  tasks: TaskData[]
}

const useGetCategories = (categoryUpdated:CategoryData) => {
  const [data, setData]: any = useState(null);
  const [loading, setLoading]: any = useState(null);
  const [error, setError] = useState(false);

  const fetchData = useCallback(async (cancelToken: CancelToken) => {
    setLoading(true);

    try {
      const categoriesList: CategoryData[] = await axios.get('http://localhost:3333/api/categories', { cancelToken });
      setData(categoriesList);
      console.log('setted data:', categoriesList);
    }
    catch (err: any) {
      console.error('ERROR:', err);
      setError(err);
    }
    finally {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    const source = axios.CancelToken.source();
    const { token } = source;
    console.log('fetchdata');
    fetchData(token);
    return () => {
      source.cancel();
    };
  }, [fetchData, categoryUpdated.id, categoryUpdated.name, categoryUpdated.description]);

  return { data, error, loading };
};

export default useGetCategories;
