// import axios, { CancelToken } from 'axios';
// import { useCallback, useEffect, useState } from 'react';

// export interface TaskData {
//   id: string
//   name: string
//   description: string
// }

// const useGetTasks = ({ taskUpdated, categoryId }: any) => {
//   const [data, setData]: any = useState(null);
//   const [loading, setLoading]: any = useState(null);
//   const [error, setError] = useState(false);

//   const fetchData = useCallback(async (cancelToken: CancelToken, catId:string = '') => {
//     setLoading(true);

//     try {
//       const taskList: TaskData[] = await axios.get('http://localhost:3333/api/tasks', { cancelToken });
//       setData(taskList);
//       console.log('setted data:', taskList);
//     }
//     catch (err: any) {
//       console.error('ERROR:', err);
//       setError(err);
//     }
//     finally {
//       setLoading(false);
//     }
//   }, []);

//   useEffect(() => {
//     const source = axios.CancelToken.source();
//     const { token } = source;
//     console.log('fetchdata');
//     fetchData(token, categoryId);
//     return () => {
//       source.cancel();
//     };
//   }, [fetchData, taskUpdated.id, taskUpdated.name, taskUpdated.description, categoryId]);

//   return { data, error, loading };
// };

// export default useGetTasks;
