import {
  Button, useDisclosure, Modal, ModalOverlay, ModalContent, ModalHeader, ModalCloseButton, ModalBody, FormControl, FormLabel, Input, ModalFooter,
} from '@chakra-ui/react';
import { FormEvent, useMemo, useState } from 'react';
import axios from 'axios';
import Task from './Task';
import { TaskData } from '../types/TaskData';
import categoryFactory from '../helpers/categoryFactory';

interface TasksResponse {
  data: TaskData
}

function TaskModal({ props }: any) {
  const {
    isOpen, onOpen, onClose, activeCategoryId, setCategoryUpdated,
  } = props;
  const [loading, setLoading] = useState(false);
  const [taskName, setTaskName] = useState('');
  const [taskDescription, setTaskDescription] = useState('');

  const handleSubmit = async (event: FormEvent): Promise<any> => {
    event.preventDefault();
    setLoading(true);
    try {
      const data: any = {
        name: taskName,
        description: taskDescription,
        category: { id: activeCategoryId },
      };
      const res: TasksResponse = await axios.post('http://localhost:3333/api/tasks', data);
      const taskData: TaskData = res?.data ?? {};
      // Again, messy state updater. [TODO] refactor to WHAT is updated rather than re-use category dto
      setCategoryUpdated(categoryFactory(activeCategoryId, taskData.id, '', []));
      onClose();
    }
    catch (e) {
      //
      console.error('THERE IS ERR', e);
    }
  };

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
    >
      <ModalOverlay />
      <ModalContent>
        <form onSubmit={handleSubmit}>
          <ModalHeader>Task</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <FormControl>
              <FormLabel>Task Name</FormLabel>
              <Input placeholder="Task Name" onChange={(event) => setTaskName(event.currentTarget.value)} />
            </FormControl>

            <FormControl mt={4}>
              <FormLabel>Task Description</FormLabel>
              <Input placeholder="Task Description" onChange={(event) => setTaskDescription(event.currentTarget.value)} />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={(e: any) => handleSubmit(e)}>
              Save
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </form>
      </ModalContent>
    </Modal>
  );
}

export default TaskModal;
