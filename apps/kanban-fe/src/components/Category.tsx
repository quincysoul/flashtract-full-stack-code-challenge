import {
  Text, Box, Button,
} from '@chakra-ui/react';
import axios from 'axios';
import { useState } from 'react';
import { useDrop } from 'react-dnd';
import categoryFactory from '../helpers/categoryFactory';
import { CategoryData } from '../hooks/useGetCategories';
import { TaskData } from '../types/TaskData';
import Task from './Task';

interface UpdateTaskDto {
  id: string;
  name?: string;
  description?: string;
  category?: Partial<CategoryData>;
}

interface CategoryDataProps extends CategoryData {
  setCategoryUpdated: (res: CategoryData) => void
  setActiveCategoryId: (uuid:string) => void
  onOpenTaskModal: () => void
  categoriesLoading: boolean
}

function Category({ props }: any) {
  const {
    id, name, tasks, description, setCategoryUpdated, setActiveCategoryId, onOpenTaskModal,
    categoriesLoading,
  }: CategoryDataProps = props;

  const handleAddTask = () => {
    setActiveCategoryId(id);
    onOpenTaskModal();
  };

  const moveTaskToCategory = (async (taskId: string) => {
    console.log('attempting drop task to THIS categoryId:', id, 'taskId:', taskId);
    let res: any = categoryFactory();
    try {
      const data: UpdateTaskDto = {
        id: taskId,
        category: { id },
      };
      res = await axios.patch(`http://localhost:3333/api/tasks/${taskId}`, data);
      console.log('RES FOR THAT:', res);
      if ((res?.data?.affected ?? 0) > 0) {
        // TODO: Messy but works, we mod id to taskId since tasks is not updated, unless
        // we fetch the tasks for `categoryId` after updating it. The patch res only gives
        // num records chg.
        setCategoryUpdated({
          id: taskId, name, description, tasks,
        });
      }
    }
    catch (e) {
      console.error('Todo: handle error in Category component,', e);
    }
    finally {
      setCategoryUpdated({
        id: taskId, name, description, tasks,
      });
    }
  });

  const [{ isOver }, drop]: any = useDrop(() => ({
    accept: 'Task',
    drop: (item: TaskData) => moveTaskToCategory(item.id),
    collect: ((monitor) => ({
      isOver: !!monitor.isOver(),
    })),
  }));

  const renderTasks = (taskList: TaskData[]) => taskList.map((task: any) => (
    <Task key={task.id} props={{ ...task, setCategoryUpdated, categoriesLoading }} />
  ));
  return (
    <Box
      // eslint-disable-next-line no-nested-ternary
      className={isOver ? 'categoryDragOver' : (!categoriesLoading ? 'category' : 'categoryDragOver')}
      alignItems="center"
      p="0.3em"
      ref={drop}
    >
      <Text fontSize="3xl" mt="0.5em" align="center">{name}</Text>
      <Button className="addTask" onClick={handleAddTask}>+ Add task</Button>
      <div className="taskStack">
        {tasks?.length > 0 ?? [] ? renderTasks(tasks) : null}
      </div>

    </Box>
  );
}

export default Category;
