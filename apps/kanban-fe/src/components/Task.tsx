import { Text } from '@chakra-ui/react';
import { ChevronLeftIcon, ChevronRightIcon, CloseIcon } from '@chakra-ui/icons';
import { useDrag } from 'react-dnd';
import axios from 'axios';
import { TaskData } from '../types/TaskData';
import { CategoryData } from '../hooks/useGetCategories';

interface TaskDataProps extends TaskData {
  setCategoryUpdated: (res: CategoryData) => void
  categoriesLoading: boolean
}

function Task({ props }: any) {
  const {
    id, name, description, setCategoryUpdated, categoriesLoading,
  }: TaskDataProps = props;
  const [{ isDragging }, drag] = useDrag(() => ({
    type: 'Task',
    item: { id },
    collect: (monitor) => ({
      isDragging: !!monitor.isDragging(),
    }),
  }));
  const deleteTask = (async () => {
    let res = null;
    try {
      res = await axios.delete(`http://localhost:3333/api/tasks/${id}`);
      if ((res?.data?.affected ?? 0) > 0) {
        setCategoryUpdated({
          id, name, description, tasks: [],
        });
      }
    }
    catch (e) {
      console.error('Todo: handle error in Category component,', e);
    }
    finally {
      //
    }
  });
  return (
    <div
      ref={categoriesLoading ? null : drag}
      // eslint-disable-next-line no-nested-ternary
      className={!isDragging ? 'task' : (!categoriesLoading ? 'draggingTask' : 'task')}
    >
      <Text fontSize="2xl">
        {name}
        <span style={{ float: 'right', marginRight: '0.5em' }}>
          {' '}
          {/* <ChevronLeftIcon />
          <ChevronRightIcon /> */}
          <CloseIcon boxSize={3} ml="0.4em" onClick={deleteTask} />
        </span>

      </Text>
      <p>{description}</p>
    </div>
  );
}

export default Task;
