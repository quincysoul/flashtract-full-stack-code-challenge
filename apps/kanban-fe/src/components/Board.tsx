import {
  Container, Heading, SimpleGrid, useDisclosure, Button, Skeleton,
} from '@chakra-ui/react';
import { useState } from 'react';
import useGetCategories from '../hooks/useGetCategories';
import Category from './Category';
import CategoryModal from './CategoryModal';
import TaskModal from './TaskModal';

function Board() {
  const [categoryUpdated, setCategoryUpdated] = useState({
    id: '', name: '', description: '', tasks: [],
  });
  const [activeCategoryId, setActiveCategoryId] = useState('');
  const {
    data: categoriesData, loading: categoriesLoading,
    error: categoriesErr,
  } = useGetCategories(categoryUpdated);
  const {
    isOpen: isOpenCategoryModal,
    onOpen: onOpenCategoryModal, onClose: onCloseCategoryModal,
  } = useDisclosure();
  const {
    isOpen: isOpenTaskModal, onOpen: onOpenTaskModal,
    onClose: onCloseTaskModal,
  } = useDisclosure();

  const renderCategories = (categories: any) => {
    console.log('RENDERCATEGORIES:', categories);
    return categories.map((category: any[any]) => (
      <Category
        key={category.id}
        props={{
          ...category, setCategoryUpdated, onOpenTaskModal, setActiveCategoryId, categoriesLoading,
        }}
      />
    ));
  };

  return (
    <Container maxW="90%" centerContent>
      <CategoryModal props={{
        isOpen: isOpenCategoryModal,
        onOpen: onOpenCategoryModal,
        onClose: onCloseCategoryModal,
        setCategoryUpdated,
      }}
      />
      <TaskModal props={{
        isOpen: isOpenTaskModal,
        onOpen: onOpenTaskModal,
        onClose: onCloseTaskModal,
        setCategoryUpdated,
        activeCategoryId,
      }}
      />
      <Heading as="h1" size="xl" pt="4em" mb="0.6em">Kanban board</Heading>
      <Button mb="0.2em" className="addTask" onClick={onOpenCategoryModal}>+ Add Category</Button>
      <SimpleGrid columns={{ sm: 2, md: 2, lg: 5 }} spacing="1em">
        { (categoriesData?.data ?? null) ? renderCategories(categoriesData.data) : null}
      </SimpleGrid>
    </Container>
  );
}

export default Board;
