import {
  Button, useDisclosure, Modal, ModalOverlay, ModalContent, ModalHeader, ModalCloseButton, ModalBody, FormControl, FormLabel, Input, ModalFooter,
} from '@chakra-ui/react';
import { FormEvent, useMemo, useState } from 'react';
import axios from 'axios';
import Task from './Task';

type CategoryData = {
  id: string,
  name: string,
  description: string,
  order: number | null
};
interface CategoriesResponse {
  data: CategoryData
}

function CategoryModal({ props }: any) {
  const {
    isOpen, onOpen, onClose, setCategoryUpdated,
  } = props;
  const [loading, setLoading] = useState(false);
  const [categoryName, setCategoryName] = useState('');
  const [categoryDescription, setCategoryDescription] = useState('');

  const handleSubmit = async (event: FormEvent): Promise<any> => {
    event.preventDefault();
    setLoading(true);
    try {
      // awai
      const res: CategoriesResponse = await axios.post('http://localhost:3333/api/categories', {
        name: categoryName,
        description: categoryDescription,
      });
      const categoryData: CategoryData = res?.data ?? {};
      setCategoryUpdated(categoryData);
      onClose();
    }
    catch (e) {
      //
      console.error('THERE IS ERR', e);
    }
  };

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
    >
      <ModalOverlay />
      <ModalContent>
        <form onSubmit={handleSubmit}>
          <ModalHeader>Create new Category</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <FormControl>
              <FormLabel>Category Name</FormLabel>
              <Input placeholder="Category Name" onChange={(event) => setCategoryName(event.currentTarget.value)} />
            </FormControl>

            <FormControl mt={4}>
              <FormLabel>Category Description</FormLabel>
              <Input placeholder="Category Description" onChange={(event) => setCategoryDescription(event.currentTarget.value)} />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={(e: any) => handleSubmit(e)}>
              Save
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </form>
      </ModalContent>
    </Modal>
  );
}

export default CategoryModal;
