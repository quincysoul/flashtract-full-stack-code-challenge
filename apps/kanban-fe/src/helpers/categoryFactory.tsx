import { CategoryData } from '../hooks/useGetCategories';

const categoryFactory = (id = '', name = '', description = '', tasks:any = []): CategoryData => ({
  id, name, description, tasks,
});

export default categoryFactory;
